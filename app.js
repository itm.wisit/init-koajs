const Koa = require('koa');
const logger = require('koa-logger');
const Router = require('koa-router');
const render = require("koa-ejs");
const path = require("path");

const app = new Koa();

render(app, {
  root: path.join(__dirname, "views"),
  layout: "template",
  viewExt: "html",
  cache: false,
  debug: false,
  async: true
});

// log all events to the terminal
app.use(logger());

// error handling
app.use(async (ctx, next) => {
  try {
    await next();
  } catch (err) {
    ctx.status = err.status || 500;
    ctx.body = err.message;
    ctx.app.emit('error', err, ctx);
  }
});

// instantiate our new Router
const router = new Router();
const dogRouter = new Router({
  prefix: '/dogs'
});
// require our external routes and pass in the router
// require('./routes/basic')({ router });
// require('./routes/dogs')({ dogRouter });


const users = ["Virat", "Sachin", "Rohit", "Dhoni"];

router.get("/users", async ctx => {
  // await ctx.render("index", {
  //   users: users
  // });
  ctx.body = users
});

router.post("/pk/:id", ctx => {
  ctx.body = ctx.request.req;
  return (ctx.status = 201);
});

router.get('/', (ctx, next) => {
  ctx.body = 'Hello World!';
 });

// tells the router to use all the routes that are on the object
app.use(router.routes());
app.use(router.allowedMethods());

// app.use(dogRouter.routes());
// app.use(dogRouter.allowedMethods());

// tell the server to listen to events on a specific port
const server = app.listen(3000);
module.exports = server;