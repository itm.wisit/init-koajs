
const Koa = require('koa');
const Router = require('koa-router');
const bodyParser = require('koa-bodyparser');
const request = require('superagent');

const app = new Koa();
const router = new Router();

app.use(bodyParser());


const line = require('@line/bot-sdk');

const latest = require('./statics/latest.json')
const _ = require('lodash')

// create LINE SDK config from env variables
const config = {
  channelAccessToken: 'vroG+jza5xQGwLy+qD9xJ+d0LdvO0G1gMm17LToEvcDHJVmOGgfLm1DRx6Huoruh4FYJ256X4Vtmcpy9+AMjhovJ5o9LtWTh1eM6lBEnrYWtN7dYjgTfvCXsff3YWWemun9zEAC/VguzaoX3sh/aXI9PbdgDzCFqoOLOYbqAITQ=',
  channelSecret: '3e4165964576c1cef04ba5f8cc025a78',
};

// create LINE SDK client
const client = new line.Client(config);


// register a webhook handler with middleware
app.use(bodyParser());

router.get('/test', ctx => {
  ctx.body = latest
});

router.post('/callback', async (ctx, next) => {
      await Promise
      .all(ctx.request.body.events.map(handleEvent))
      .then((result) => console.log('success'))
      .catch((err) => {
        console.error(err);
        ctx.throw(500);
      });
 });

// event handler
async function handleEvent(event) {
  console.log(event)
  if (event.type !== 'message' || event.message.type !== 'text') {
    // ignore non-text-message event
    return Promise.resolve(null);
  }
  if(event.message.text.search(' ') != -1) {
    var checkParams = event.message.text.split(' ');
    if(checkParams) {
      var amount = parseFloat(checkParams[0])
      var currency = checkParams[1]
      if(_.isUndefined(currency)){
        currency = 'USD'
      } else {
        currency = currency.toUpperCase()
        var findCurrency = _.has(latest.rates, currency)
        if(!findCurrency) {
          const echo = { type: 'text', text: 'Error. Currency Not found' };
          return client.replyMessage(event.replyToken, echo);
        }
        
      }
      if(_.isNaN(amount)) return Promise.resolve(null);
    }
  } else {
    var matches = event.message.text.match(/^([0-9]*)([a-zA-Z]{3})$/)
    var amount
    var currency
    if(matches != null) {
       amount = parseFloat(matches[1])
       currency = matches[2]
    }else {
       amount = parseFloat(event.message.text)
    }
    if(_.isUndefined(currency)){
      currency = 'USD'
    } else {
      currency = currency.toUpperCase()
      var findCurrency = _.has(latest.rates, currency)
      if(!findCurrency) {
        const echo = { type: 'text', text: 'Error. Currency Not found' };
        return client.replyMessage(event.replyToken, echo);
      }
      
    }
    if(_.isNaN(amount)) return Promise.resolve(null);
  }
  
  var result = await new Promise((resolve, reject) => {
    request
    .get('http://data.fixer.io/api/latest?access_key=fe988c1f9b323afd3eef9be86fe4b8ac&format=1')
    .then(async res => {
      if(res.body.success) {
        resolve(res.body)
      } else {
        resolve(latest)
      }
    })
    .catch(err => {
      reject(err)
    });
  })
  // if(!result.success) {
  //   result = await new Promise((resolve, reject) => {
  //     request
  //     .get('http://localhost:5000/test')
  //     .then(async res => {
  //       if(res.body.success) {
  //         resolve(res.body)
  //       }
  //     })
  //     .catch(err => {
  //       reject(err)
  //     });
  //   })
  // }

  const response = convert(result, amount, currency)
  // request
  //   // .get('http://data.fixer.io/api/latest?access_key=fe988c1f9b323afd3eef9be86fe4b8ac&format=1')
  //   .get('http://localhost:5000/test')
  //   .then(async res => {
  //     const echo = { type: 'text', text: `base: 'EUR' => USD ` + res.body.rates.USD };
      
      
  //     // use reply API
  //     return client.replyMessage(event.replyToken, echo);
  //   })
  //   .catch(err => {
  //     console.log(err);
  //   });

  const echo = { type: 'text', text: `${amount} ${currency} => ` + response.toFixed(2) + ` THB` };
  return client.replyMessage(event.replyToken, echo);

}

function convert(result, amount, currency) {
  var changeBase = 1 / result.rates[currency]
  
  return changeBase * result.rates['THB'] * amount
}

// tells the router to use all the routes that are on the object
app.use(router.routes());
app.use(router.allowedMethods());

// app.use(dogRouter.routes());
// app.use(dogRouter.allowedMethods());

// tell the server to listen to events on a specific port
const server = app.listen(5000);
module.exports = server;